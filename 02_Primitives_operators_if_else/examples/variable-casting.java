package examples;

public class variable-casting {
    public static void main( String[] args ) {
        double varDouble = 3.522;
        int varInt = (int) varDouble;
        System.out.print(varInt);
    }
}
